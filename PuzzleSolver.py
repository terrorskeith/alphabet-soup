import sys

##Parses the passed file into the necessary information to solve the puzzle
def parsePuzzleContents(fileURL):
    ##Opens file to parse line by line
    file_handler = open(fileURL, "r")

    ##Reads in the first line and parses into number of rows and columns.
    matrix_size = file_handler.readline()
    matrix_parsed = matrix_size.split('x')
    rows, columns = int(matrix_parsed[0]), int(matrix_parsed[1])

    ##Creates a effective "2D List" to dynamically fill with the contents of the word search puzzle.
    word_search_matrix = []
    for each in range(0, rows):
        word_search_matrix.append(file_handler.readline().replace('\n', '').split(' '))

    ##Create a answer key list from the remaining lines in the file.
    answer_list = []
    for answer in file_handler:
        answer_list.append(answer.replace('\n', ''))

    ##Close file
    file_handler.close()

    ##Returns the number of rows and columns in the puzzle, the puzzle, and the answer key
    return rows, columns, word_search_matrix, answer_list

##Check one of 8 cardinal directions to see if it matches the provided word
def check_direction(rows, columns, row_index, row_direction, column_index, column_direction, answer, word_search_matrix):
    ##Checks the column and row the last letter of the word will be in and evaluates if it is possible.
    row_end = row_index + ((len(answer)-1) * row_direction)
    column_end = column_index + ((len(answer)-1) * column_direction)

    ##If the word in this direction fits in the puzzle then check every letter for a match.
    ##If the letters don't match or the word doesn't fit, then return None for failure, otherwise return the coordinates.
    if row_end < rows and row_end >= 0 and column_end < columns and column_end >= 0:
        for letter in answer:
            if letter == word_search_matrix[row_index][column_index]:
                row_index += row_direction
                column_index += column_direction
            else:
                return None
        return str(row_index - row_direction) + ":" + str(column_index - column_direction)
    else:
        return None

##Looks through the word search for the provided word
def find_word(rows, columns, answer, word_search_matrix):
    ##These provide the 8 cardinal directions the word could be going for evaluation. Each element is paired to form a vector.
    row_direction = [-1, -1, -1, 0, 1, 1, 1, 0]
    column_direction = [-1, 0, 1, 1, 1, 0, -1, -1]

    ##Evaluates each row, then column to find the first letter of the word.
    for row_index in range(0, rows):
        for column_index in range(0, columns):
            ##If the letter is found proceed to check each of the 8 cardinal dirctions for matches
            if answer[0] == word_search_matrix[row_index][column_index]:
                for direction_index in range(0, 8):
                    answer_end_result = check_direction(rows, columns, row_index, row_direction[direction_index], column_index, column_direction[direction_index], answer, word_search_matrix)
                    if answer_end_result:
                        return str(row_index) + ":" + str(column_index) + " " + answer_end_result
    ##If nothing is found, return this message.
    return "Error:NotInPuzzle"

##Main Function
def main(fileURL):
    ##Retrieves the number of rows and columns, the wordsearch puzzle, and the answer list parsed from the given file
    rows, columns, word_search_matrix, answer_list = parsePuzzleContents(fileURL)

    ##Checks each answer for the existance of the answer in the word search to find the coordinates of the first and last letters.
    for answer in answer_list:
        print(answer + " " + find_word(rows, columns, answer.replace(" ", ""), word_search_matrix))

##Start of the Script. Takes one argument and utilizies it as the location and file of the solvable puzzle. Otherwise, it uses the input from the provided puzzle.txt file.
if __name__ == "__main__":
    if len(sys.argv) == 1:
        main("puzzle.txt")
    else:
        main(sys.argv[1])
